// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "MySuperGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MySuperGame, "MySuperGame" );
 