// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EGamePlayState : int32;
#ifdef MYSUPERGAME_MySuperGameGameMode_generated_h
#error "MySuperGameGameMode.generated.h already included, missing '#pragma once' in MySuperGameGameMode.h"
#endif
#define MYSUPERGAME_MySuperGameGameMode_generated_h

#define MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_SPARSE_DATA
#define MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EGamePlayState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	}


#define MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EGamePlayState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	}


#define MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMySuperGameGameMode(); \
	friend struct Z_Construct_UClass_AMySuperGameGameMode_Statics; \
public: \
	DECLARE_CLASS(AMySuperGameGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySuperGame"), MYSUPERGAME_API) \
	DECLARE_SERIALIZER(AMySuperGameGameMode)


#define MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_INCLASS \
private: \
	static void StaticRegisterNativesAMySuperGameGameMode(); \
	friend struct Z_Construct_UClass_AMySuperGameGameMode_Statics; \
public: \
	DECLARE_CLASS(AMySuperGameGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySuperGame"), MYSUPERGAME_API) \
	DECLARE_SERIALIZER(AMySuperGameGameMode)


#define MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MYSUPERGAME_API AMySuperGameGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMySuperGameGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MYSUPERGAME_API, AMySuperGameGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySuperGameGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MYSUPERGAME_API AMySuperGameGameMode(AMySuperGameGameMode&&); \
	MYSUPERGAME_API AMySuperGameGameMode(const AMySuperGameGameMode&); \
public:


#define MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MYSUPERGAME_API AMySuperGameGameMode(AMySuperGameGameMode&&); \
	MYSUPERGAME_API AMySuperGameGameMode(const AMySuperGameGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MYSUPERGAME_API, AMySuperGameGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySuperGameGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMySuperGameGameMode)


#define MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_PRIVATE_PROPERTY_OFFSET
#define MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_19_PROLOG
#define MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_PRIVATE_PROPERTY_OFFSET \
	MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_SPARSE_DATA \
	MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_RPC_WRAPPERS \
	MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_INCLASS \
	MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_PRIVATE_PROPERTY_OFFSET \
	MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_SPARSE_DATA \
	MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_INCLASS_NO_PURE_DECLS \
	MySuperGame_Source_MySuperGame_MySuperGameGameMode_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYSUPERGAME_API UClass* StaticClass<class AMySuperGameGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MySuperGame_Source_MySuperGame_MySuperGameGameMode_h


#define FOREACH_ENUM_EGAMEPLAYSTATE(op) \
	op(EGamePlayState::EPlaying) \
	op(EGamePlayState::EGameOver) \
	op(EGamePlayState::EUnknown) 

enum class EGamePlayState;
template<> MYSUPERGAME_API UEnum* StaticEnum<EGamePlayState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
