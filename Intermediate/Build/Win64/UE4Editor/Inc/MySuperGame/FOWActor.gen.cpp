// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MySuperGame/FOWActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFOWActor() {}
// Cross Module References
	MYSUPERGAME_API UClass* Z_Construct_UClass_AFOWActor_NoRegister();
	MYSUPERGAME_API UClass* Z_Construct_UClass_AFOWActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_MySuperGame();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void AFOWActor::StaticRegisterNativesAFOWActor()
	{
	}
	UClass* Z_Construct_UClass_AFOWActor_NoRegister()
	{
		return AFOWActor::StaticClass();
	}
	struct Z_Construct_UClass_AFOWActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_dynamicMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_dynamicMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_dynamicMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_dynamicMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_dynamicTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_dynamicTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_squarePlane_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_squarePlane;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFOWActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_MySuperGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFOWActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FOWActor.h" },
		{ "ModuleRelativePath", "FOWActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFOWActor_Statics::NewProp_m_dynamicMaterialInstance_MetaData[] = {
		{ "ModuleRelativePath", "FOWActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFOWActor_Statics::NewProp_m_dynamicMaterialInstance = { "m_dynamicMaterialInstance", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFOWActor, m_dynamicMaterialInstance), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFOWActor_Statics::NewProp_m_dynamicMaterialInstance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFOWActor_Statics::NewProp_m_dynamicMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFOWActor_Statics::NewProp_m_dynamicMaterial_MetaData[] = {
		{ "ModuleRelativePath", "FOWActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFOWActor_Statics::NewProp_m_dynamicMaterial = { "m_dynamicMaterial", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFOWActor, m_dynamicMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFOWActor_Statics::NewProp_m_dynamicMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFOWActor_Statics::NewProp_m_dynamicMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFOWActor_Statics::NewProp_m_dynamicTexture_MetaData[] = {
		{ "ModuleRelativePath", "FOWActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFOWActor_Statics::NewProp_m_dynamicTexture = { "m_dynamicTexture", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFOWActor, m_dynamicTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFOWActor_Statics::NewProp_m_dynamicTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFOWActor_Statics::NewProp_m_dynamicTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFOWActor_Statics::NewProp_m_squarePlane_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "FOWActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFOWActor_Statics::NewProp_m_squarePlane = { "m_squarePlane", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFOWActor, m_squarePlane), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFOWActor_Statics::NewProp_m_squarePlane_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFOWActor_Statics::NewProp_m_squarePlane_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AFOWActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFOWActor_Statics::NewProp_m_dynamicMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFOWActor_Statics::NewProp_m_dynamicMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFOWActor_Statics::NewProp_m_dynamicTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFOWActor_Statics::NewProp_m_squarePlane,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFOWActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFOWActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFOWActor_Statics::ClassParams = {
		&AFOWActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AFOWActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AFOWActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AFOWActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFOWActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFOWActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFOWActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFOWActor, 2555951816);
	template<> MYSUPERGAME_API UClass* StaticClass<AFOWActor>()
	{
		return AFOWActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFOWActor(Z_Construct_UClass_AFOWActor, &AFOWActor::StaticClass, TEXT("/Script/MySuperGame"), TEXT("AFOWActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFOWActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
