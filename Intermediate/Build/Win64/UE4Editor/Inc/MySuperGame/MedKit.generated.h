// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef MYSUPERGAME_MedKit_generated_h
#error "MedKit.generated.h already included, missing '#pragma once' in MedKit.h"
#endif
#define MYSUPERGAME_MedKit_generated_h

#define MySuperGame_Source_MySuperGame_MedKit_h_13_SPARSE_DATA
#define MySuperGame_Source_MySuperGame_MedKit_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnOverlap) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_MyOverlappedActor); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlap(Z_Param_MyOverlappedActor,Z_Param_OtherActor); \
		P_NATIVE_END; \
	}


#define MySuperGame_Source_MySuperGame_MedKit_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnOverlap) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_MyOverlappedActor); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlap(Z_Param_MyOverlappedActor,Z_Param_OtherActor); \
		P_NATIVE_END; \
	}


#define MySuperGame_Source_MySuperGame_MedKit_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMedKit(); \
	friend struct Z_Construct_UClass_AMedKit_Statics; \
public: \
	DECLARE_CLASS(AMedKit, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySuperGame"), NO_API) \
	DECLARE_SERIALIZER(AMedKit)


#define MySuperGame_Source_MySuperGame_MedKit_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAMedKit(); \
	friend struct Z_Construct_UClass_AMedKit_Statics; \
public: \
	DECLARE_CLASS(AMedKit, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySuperGame"), NO_API) \
	DECLARE_SERIALIZER(AMedKit)


#define MySuperGame_Source_MySuperGame_MedKit_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMedKit(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMedKit) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMedKit); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMedKit); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMedKit(AMedKit&&); \
	NO_API AMedKit(const AMedKit&); \
public:


#define MySuperGame_Source_MySuperGame_MedKit_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMedKit(AMedKit&&); \
	NO_API AMedKit(const AMedKit&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMedKit); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMedKit); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMedKit)


#define MySuperGame_Source_MySuperGame_MedKit_h_13_PRIVATE_PROPERTY_OFFSET
#define MySuperGame_Source_MySuperGame_MedKit_h_10_PROLOG
#define MySuperGame_Source_MySuperGame_MedKit_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySuperGame_Source_MySuperGame_MedKit_h_13_PRIVATE_PROPERTY_OFFSET \
	MySuperGame_Source_MySuperGame_MedKit_h_13_SPARSE_DATA \
	MySuperGame_Source_MySuperGame_MedKit_h_13_RPC_WRAPPERS \
	MySuperGame_Source_MySuperGame_MedKit_h_13_INCLASS \
	MySuperGame_Source_MySuperGame_MedKit_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MySuperGame_Source_MySuperGame_MedKit_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySuperGame_Source_MySuperGame_MedKit_h_13_PRIVATE_PROPERTY_OFFSET \
	MySuperGame_Source_MySuperGame_MedKit_h_13_SPARSE_DATA \
	MySuperGame_Source_MySuperGame_MedKit_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	MySuperGame_Source_MySuperGame_MedKit_h_13_INCLASS_NO_PURE_DECLS \
	MySuperGame_Source_MySuperGame_MedKit_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYSUPERGAME_API UClass* StaticClass<class AMedKit>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MySuperGame_Source_MySuperGame_MedKit_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
