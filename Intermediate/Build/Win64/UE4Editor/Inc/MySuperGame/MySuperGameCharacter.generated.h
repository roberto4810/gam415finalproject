// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYSUPERGAME_MySuperGameCharacter_generated_h
#error "MySuperGameCharacter.generated.h already included, missing '#pragma once' in MySuperGameCharacter.h"
#endif
#define MYSUPERGAME_MySuperGameCharacter_generated_h

#define MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_SPARSE_DATA
#define MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPlayFlash) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->PlayFlash(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetMagicChange) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_MagicChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetMagicChange(Z_Param_MagicChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetMagicState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetMagicState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetMagicValue) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetMagicValue(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetDamageState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetDamageState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDamageTimer) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DamageTimer(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMagicIntText) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->GetMagicIntText(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetHealthIntText) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->GetHealthIntText(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateMagic) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_MagicChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateMagic(Z_Param_MagicChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateHealth) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_HealthChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateHealth(Z_Param_HealthChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMagic) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetMagic(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetHealth(); \
		P_NATIVE_END; \
	}


#define MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPlayFlash) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->PlayFlash(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetMagicChange) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_MagicChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetMagicChange(Z_Param_MagicChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetMagicState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetMagicState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetMagicValue) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetMagicValue(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetDamageState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetDamageState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDamageTimer) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DamageTimer(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMagicIntText) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->GetMagicIntText(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetHealthIntText) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->GetHealthIntText(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateMagic) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_MagicChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateMagic(Z_Param_MagicChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateHealth) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_HealthChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateHealth(Z_Param_HealthChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMagic) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetMagic(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetHealth(); \
		P_NATIVE_END; \
	}


#define MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMySuperGameCharacter(); \
	friend struct Z_Construct_UClass_AMySuperGameCharacter_Statics; \
public: \
	DECLARE_CLASS(AMySuperGameCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySuperGame"), NO_API) \
	DECLARE_SERIALIZER(AMySuperGameCharacter)


#define MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAMySuperGameCharacter(); \
	friend struct Z_Construct_UClass_AMySuperGameCharacter_Statics; \
public: \
	DECLARE_CLASS(AMySuperGameCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySuperGame"), NO_API) \
	DECLARE_SERIALIZER(AMySuperGameCharacter)


#define MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMySuperGameCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMySuperGameCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMySuperGameCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySuperGameCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMySuperGameCharacter(AMySuperGameCharacter&&); \
	NO_API AMySuperGameCharacter(const AMySuperGameCharacter&); \
public:


#define MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMySuperGameCharacter(AMySuperGameCharacter&&); \
	NO_API AMySuperGameCharacter(const AMySuperGameCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMySuperGameCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySuperGameCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMySuperGameCharacter)


#define MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(AMySuperGameCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(AMySuperGameCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(AMySuperGameCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(AMySuperGameCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(AMySuperGameCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AMySuperGameCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(AMySuperGameCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(AMySuperGameCharacter, L_MotionController); }


#define MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_13_PROLOG
#define MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_SPARSE_DATA \
	MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_RPC_WRAPPERS \
	MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_INCLASS \
	MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_SPARSE_DATA \
	MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_INCLASS_NO_PURE_DECLS \
	MySuperGame_Source_MySuperGame_MySuperGameCharacter_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYSUPERGAME_API UClass* StaticClass<class AMySuperGameCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MySuperGame_Source_MySuperGame_MySuperGameCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
