// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef MYSUPERGAME_MySuperGameProjectile_generated_h
#error "MySuperGameProjectile.generated.h already included, missing '#pragma once' in MySuperGameProjectile.h"
#endif
#define MYSUPERGAME_MySuperGameProjectile_generated_h

#define MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_SPARSE_DATA
#define MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMySuperGameProjectile(); \
	friend struct Z_Construct_UClass_AMySuperGameProjectile_Statics; \
public: \
	DECLARE_CLASS(AMySuperGameProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySuperGame"), NO_API) \
	DECLARE_SERIALIZER(AMySuperGameProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMySuperGameProjectile(); \
	friend struct Z_Construct_UClass_AMySuperGameProjectile_Statics; \
public: \
	DECLARE_CLASS(AMySuperGameProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySuperGame"), NO_API) \
	DECLARE_SERIALIZER(AMySuperGameProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMySuperGameProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMySuperGameProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMySuperGameProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySuperGameProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMySuperGameProjectile(AMySuperGameProjectile&&); \
	NO_API AMySuperGameProjectile(const AMySuperGameProjectile&); \
public:


#define MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMySuperGameProjectile(AMySuperGameProjectile&&); \
	NO_API AMySuperGameProjectile(const AMySuperGameProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMySuperGameProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySuperGameProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMySuperGameProjectile)


#define MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(AMySuperGameProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(AMySuperGameProjectile, ProjectileMovement); }


#define MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_9_PROLOG
#define MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_SPARSE_DATA \
	MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_RPC_WRAPPERS \
	MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_INCLASS \
	MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_SPARSE_DATA \
	MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_INCLASS_NO_PURE_DECLS \
	MySuperGame_Source_MySuperGame_MySuperGameProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYSUPERGAME_API UClass* StaticClass<class AMySuperGameProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MySuperGame_Source_MySuperGame_MySuperGameProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
