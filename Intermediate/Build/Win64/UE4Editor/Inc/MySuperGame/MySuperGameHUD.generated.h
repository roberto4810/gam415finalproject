// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYSUPERGAME_MySuperGameHUD_generated_h
#error "MySuperGameHUD.generated.h already included, missing '#pragma once' in MySuperGameHUD.h"
#endif
#define MYSUPERGAME_MySuperGameHUD_generated_h

#define MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_SPARSE_DATA
#define MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_RPC_WRAPPERS
#define MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMySuperGameHUD(); \
	friend struct Z_Construct_UClass_AMySuperGameHUD_Statics; \
public: \
	DECLARE_CLASS(AMySuperGameHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySuperGame"), NO_API) \
	DECLARE_SERIALIZER(AMySuperGameHUD)


#define MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMySuperGameHUD(); \
	friend struct Z_Construct_UClass_AMySuperGameHUD_Statics; \
public: \
	DECLARE_CLASS(AMySuperGameHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySuperGame"), NO_API) \
	DECLARE_SERIALIZER(AMySuperGameHUD)


#define MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMySuperGameHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMySuperGameHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMySuperGameHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySuperGameHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMySuperGameHUD(AMySuperGameHUD&&); \
	NO_API AMySuperGameHUD(const AMySuperGameHUD&); \
public:


#define MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMySuperGameHUD(AMySuperGameHUD&&); \
	NO_API AMySuperGameHUD(const AMySuperGameHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMySuperGameHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySuperGameHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMySuperGameHUD)


#define MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__HUDWidgetClass() { return STRUCT_OFFSET(AMySuperGameHUD, HUDWidgetClass); } \
	FORCEINLINE static uint32 __PPO__CurrentWidget() { return STRUCT_OFFSET(AMySuperGameHUD, CurrentWidget); }


#define MySuperGame_Source_MySuperGame_MySuperGameHUD_h_9_PROLOG
#define MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_SPARSE_DATA \
	MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_RPC_WRAPPERS \
	MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_INCLASS \
	MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_SPARSE_DATA \
	MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_INCLASS_NO_PURE_DECLS \
	MySuperGame_Source_MySuperGame_MySuperGameHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYSUPERGAME_API UClass* StaticClass<class AMySuperGameHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MySuperGame_Source_MySuperGame_MySuperGameHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
