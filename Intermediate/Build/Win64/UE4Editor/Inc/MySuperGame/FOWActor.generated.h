// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYSUPERGAME_FOWActor_generated_h
#error "FOWActor.generated.h already included, missing '#pragma once' in FOWActor.h"
#endif
#define MYSUPERGAME_FOWActor_generated_h

#define MySuperGame_Source_MySuperGame_FOWActor_h_13_SPARSE_DATA
#define MySuperGame_Source_MySuperGame_FOWActor_h_13_RPC_WRAPPERS
#define MySuperGame_Source_MySuperGame_FOWActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define MySuperGame_Source_MySuperGame_FOWActor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFOWActor(); \
	friend struct Z_Construct_UClass_AFOWActor_Statics; \
public: \
	DECLARE_CLASS(AFOWActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySuperGame"), NO_API) \
	DECLARE_SERIALIZER(AFOWActor)


#define MySuperGame_Source_MySuperGame_FOWActor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAFOWActor(); \
	friend struct Z_Construct_UClass_AFOWActor_Statics; \
public: \
	DECLARE_CLASS(AFOWActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySuperGame"), NO_API) \
	DECLARE_SERIALIZER(AFOWActor)


#define MySuperGame_Source_MySuperGame_FOWActor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFOWActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFOWActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFOWActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFOWActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFOWActor(AFOWActor&&); \
	NO_API AFOWActor(const AFOWActor&); \
public:


#define MySuperGame_Source_MySuperGame_FOWActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFOWActor(AFOWActor&&); \
	NO_API AFOWActor(const AFOWActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFOWActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFOWActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFOWActor)


#define MySuperGame_Source_MySuperGame_FOWActor_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__m_squarePlane() { return STRUCT_OFFSET(AFOWActor, m_squarePlane); } \
	FORCEINLINE static uint32 __PPO__m_dynamicTexture() { return STRUCT_OFFSET(AFOWActor, m_dynamicTexture); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterial() { return STRUCT_OFFSET(AFOWActor, m_dynamicMaterial); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterialInstance() { return STRUCT_OFFSET(AFOWActor, m_dynamicMaterialInstance); }


#define MySuperGame_Source_MySuperGame_FOWActor_h_10_PROLOG
#define MySuperGame_Source_MySuperGame_FOWActor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySuperGame_Source_MySuperGame_FOWActor_h_13_PRIVATE_PROPERTY_OFFSET \
	MySuperGame_Source_MySuperGame_FOWActor_h_13_SPARSE_DATA \
	MySuperGame_Source_MySuperGame_FOWActor_h_13_RPC_WRAPPERS \
	MySuperGame_Source_MySuperGame_FOWActor_h_13_INCLASS \
	MySuperGame_Source_MySuperGame_FOWActor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MySuperGame_Source_MySuperGame_FOWActor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySuperGame_Source_MySuperGame_FOWActor_h_13_PRIVATE_PROPERTY_OFFSET \
	MySuperGame_Source_MySuperGame_FOWActor_h_13_SPARSE_DATA \
	MySuperGame_Source_MySuperGame_FOWActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	MySuperGame_Source_MySuperGame_FOWActor_h_13_INCLASS_NO_PURE_DECLS \
	MySuperGame_Source_MySuperGame_FOWActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYSUPERGAME_API UClass* StaticClass<class AFOWActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MySuperGame_Source_MySuperGame_FOWActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
