// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYSUPERGAME_CubeActor_generated_h
#error "CubeActor.generated.h already included, missing '#pragma once' in CubeActor.h"
#endif
#define MYSUPERGAME_CubeActor_generated_h

#define MySuperGame_Source_MySuperGame_CubeActor_h_13_SPARSE_DATA
#define MySuperGame_Source_MySuperGame_CubeActor_h_13_RPC_WRAPPERS
#define MySuperGame_Source_MySuperGame_CubeActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define MySuperGame_Source_MySuperGame_CubeActor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACubeActor(); \
	friend struct Z_Construct_UClass_ACubeActor_Statics; \
public: \
	DECLARE_CLASS(ACubeActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySuperGame"), NO_API) \
	DECLARE_SERIALIZER(ACubeActor)


#define MySuperGame_Source_MySuperGame_CubeActor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesACubeActor(); \
	friend struct Z_Construct_UClass_ACubeActor_Statics; \
public: \
	DECLARE_CLASS(ACubeActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySuperGame"), NO_API) \
	DECLARE_SERIALIZER(ACubeActor)


#define MySuperGame_Source_MySuperGame_CubeActor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACubeActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACubeActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACubeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACubeActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACubeActor(ACubeActor&&); \
	NO_API ACubeActor(const ACubeActor&); \
public:


#define MySuperGame_Source_MySuperGame_CubeActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACubeActor(ACubeActor&&); \
	NO_API ACubeActor(const ACubeActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACubeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACubeActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACubeActor)


#define MySuperGame_Source_MySuperGame_CubeActor_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__mesh() { return STRUCT_OFFSET(ACubeActor, mesh); }


#define MySuperGame_Source_MySuperGame_CubeActor_h_10_PROLOG
#define MySuperGame_Source_MySuperGame_CubeActor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySuperGame_Source_MySuperGame_CubeActor_h_13_PRIVATE_PROPERTY_OFFSET \
	MySuperGame_Source_MySuperGame_CubeActor_h_13_SPARSE_DATA \
	MySuperGame_Source_MySuperGame_CubeActor_h_13_RPC_WRAPPERS \
	MySuperGame_Source_MySuperGame_CubeActor_h_13_INCLASS \
	MySuperGame_Source_MySuperGame_CubeActor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MySuperGame_Source_MySuperGame_CubeActor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySuperGame_Source_MySuperGame_CubeActor_h_13_PRIVATE_PROPERTY_OFFSET \
	MySuperGame_Source_MySuperGame_CubeActor_h_13_SPARSE_DATA \
	MySuperGame_Source_MySuperGame_CubeActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	MySuperGame_Source_MySuperGame_CubeActor_h_13_INCLASS_NO_PURE_DECLS \
	MySuperGame_Source_MySuperGame_CubeActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYSUPERGAME_API UClass* StaticClass<class ACubeActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MySuperGame_Source_MySuperGame_CubeActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
